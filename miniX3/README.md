# MiniX3: Throbber

### RunME

<img src="miniX3.png" width=windowWidth height=windowHeight>

Her er min [miniX3](https://bustersiem.gitlab.io/aestetisk-programmering/miniX3/index.html) og det tilhørende link til min [source code](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX3/minix3sketch.js).

Til at lægge ud med vil jeg påpege, at jeg har taget inspiration og lånt måne orbit-koden fra Kevin Workman, hvis video er fundet på YouTube. Derudover vil jeg **prøve** at holde denne del med koden lidt kort, derfor beskriver jeg kun orbit-delen af koden, da de tekst-baserede dele virker lidt overflødigt at dissekere bid for bid!

Jeg startede med at deklarere variablen `angle`, `moonSize` og `orbitDis` så jeg eventuelt ville kunne kalde dem i min `moonThrob` funktion. `moonSize` bestemmer størrelsen på månen (som reelt består af otte emojier), mens `orbitDis` er afstanden fra 0, 0 punktet på mit canvas. Månen består som sagt af otte forskellige emojier i en array, og for at min array overhovedet kan loope, lavede jeg derfor også en index-variabel, som jeg kaldte `moonI`. `moons` variablen er en string-array med otte tekst-"dele" i, som kan ses her: `let moons = ['🌕', '🌖', '🌗', '🌘', '🌑', '🌒', '🌓', '🌔'];`.

I  `moonThrob` funktionen er koden (som jeg har sat ind herunder), der får måne-throbberen til at rotere og "køre" rundt: 

```
textAlign(CENTER, CENTER);
angle++;

if (angle % 45 == 0) {
    moonI++;
    if (moonI >= moons.length) {
        moonI = 0;
    }
}

let moonX =  width/2 + cos(radians(angle)) * orbitDis;
let moonY = height/2 + sin(radians(angle)) * orbitDis;

textSize(moonSize);
text(moons[moonI], moonX, moonY-20);
```

Koden starter ud med at definere fra hvilket punkt månernes (som teknisk set er et stykke tekst i form af et emoji) startposition skal være. Den præ-definerede indstilling er fra venstre hjørne af det første bogstav, men det ændrede jeg til at være i midten af den runde emoji ved hjælp af `textAlign`. Derefter *incrementer* (++-funktionen) `angle` og dernæst kan man se mit `if`-statement, som styrer hvor hurtigt måne-faserne skifter, det vil sige; hvor hurtigt `if`-statementet looper igennem `moonI`. Dette `if`-statement bliver kun kørt når *remainderen* af `angle` er lig med 0. Jeg har brugt en `modulo`-operateren, der i mit tilfælde dividerer `angle` med 45. Tallet 45 er valgt fordi der er otte måner - og de otte måner kører i et kredsløb, der er cirkulært, det betyder derfor at hver måne-fase skal have lov til at blive vist én gang hver på de 360 grader som kredsløbet har (for de hurtige er det 360 grader divideret med 8 faser hvilket giver 45 grader for hver fase). `moonI` bliver derfor incrementet med én, hver gang månen har kørt 45 grader - hvilket gør at et nyt emoji bliver vidst!

Det *nestede* `if`-statement sørger for, at de forskellige måner i `moons` variablen kører i en cyklus, så når den ottende "unit" i min array (det vil sige moons[7]) er nået, så starter `moonI` forfra (denne variabel definerer, som sagt, hvilken måne der bliver vist).

Variablerne `moonX` og `moonY` bruges både til at definere månens startende y- og x-position, men de bruges også til, at vinklen på månens kredsløb kan ændre sig over tid. Rotationen på henholdsvis `moonX` og `moonY` defineres med cosinus af `angle` (som jeg lavede til en variabel i det globale scope) og med sinus af den samme vinkel ganget med afstanden `orbitDis`. En lille disclaimer: Jeg er ikke den aller skarpeste til matematikken bag det her, men jeg skal gøre mit bedste for at forklare de her to bidder kode. `cos(radians())`-vinkelen giver rotationen for månen med uret på y-aksen, mens `sin(radians())`-vinkelen giver (sjovt nok) rotationen for månen med uret på x-aksen.

Til sidst kalder jeg `textSize` (der egentlig bestemmes i toppen af koden det globale scope), for at bestemme størrelsen på månen. Og `text` tager argumentet `moons` (som er min array med de otte forskellige måner) med `moonI`, som er index-værdien, der looper igennem i `if`-statementet. `text` tager også argumenterne for x- og y-postionen for månen, som jeg uddybte før.

Det var en lidt snørklet beskrivelse, men jeg håber at det meste har givet mening i sidste ende!

### ReadMe

I denne miniX tænkte jeg, at jeg gerne ville udforske noget om den påvirking og det aftryk som teknologi har på klimaet. Jeg har for nyligt fået øjnene op for at nyhedsmails (og emails generelt) bare i sig selv [sætter et CO2-aftryk på klimaet](https://www.bbc.com/news/technology-55002423 "BBC: Climate change: Can sending fewer emails really save the planet?"). Det fik mig til at tænke på, at jeg selv (som den "gamer", hvilket jeg nok må indrømme, at jeg er) højst sandsynligt også er en stor synder i form af al den strøm, der går til mit maskineri når jeg spiller. Jeg er dog også klar over at det selvfølgelig ikke kun er spilleglade gamere, der genererer de her CO2-udledninger. Der bliver for eksempel også udledt carbon ved hverdagsting som bare internet, TV- og film-streaming, osv. <br>
Jeg ville derfor sætte fokus på dette ved, at min throbber skulle ligne Jorden, der gradvist, skulle blive mere og mere blå - som et bogstaveligt "symbol" på vandet der oversvømmer os. Dette er egentlig meget relavant i lyset af at [Venedig i de seneste år har haft så høje vandstande](https://www.forbes.com/sites/rebeccahughes/2022/12/27/the-glass-barriers-saving-venices-900-year-old-church-from-flooding/ "Forbes: The Glass Barriers Saving Venice’s 900-Year-Old Church From Flooding"), at byen næsten er blevet oversvømmet; og at kanalerne i byen nu (på den helt anden side af spektrummet) [næsten er udtørrede](https://www.reuters.com/world/europe/italy-faces-new-drought-alert-after-another-dry-winter-2023-02-20/ "Reuters: Italy faces new drought alert as Venice canals run dry"). Dog lod mine ambitioner tage lidt overhånd, da jeg rimelig hurtigt indså at det ikke var muligt - f.eks. kan man ikke "tegne" på objekter i `WEBGL`-mode, så jeg kunne ikke engang kreere noget der bare lignede Jorden en lille smule... <br> 

Jeg blev derfor nødt til at "*scramble*" en lille smule, og søgte derfra lidt inspiration i forelæsningerne og teksterne. MEN! I sidste ende fandt jeg ud af, at det måske ville være bedre at fokusere mere på det med computer-tid og tid generelt (så havde jeg også et afsæt i teksterne, som jeg kunne refererer til)! Så jeg har i denne miniX taget udgangspunkt i en passage fra side 92 i Soon & Coxs Aesthetic Programming, som jeg egentlig synes er meget passende:

> 	"(…) machine-time operates at a different register from human-time, further complicated by global network infrastructures, and notions of real-time computation."

Som der siges i citatet så er machine-time ikke helt den samme tid som vi mennesker kender til, og så tænkte jeg at det ville være interessant at lave en throbber, der ligesom 'informerede' brugerne om at computeren tager sig en slapper! For hvem ved? Måske er det egentlig bare computerens luskede måde at tage en på øjet, når den begynder med dens buffering og delaying...

I forhold til tids-relaterede syntakser i koden, har jeg jo selvfølgelig `frameRate`, der bestemmer hvor hurtigt månens kredsløb på skærmen skal være - jeg kan styre om den skal suse rundt, på 60 frames, eller om den skal være laaangsom og meget "*sleepy*", og kun køre 10 frames i sekundet. Jeg tænkte egentlig bare at min throbber skulle afspejle den helt klassiske throbbers hastighed; det var ikke fordi jeg tænkte så meget over det, men det kan godt være lidt hårdt (måske endda lidt kvalmende?) at kigge på sådan en cirkel der farer rundt!

Som vi snakkede om til forelæsningen, hvor vi blev introduceret til denne miniX, så tror jeg folk har forskellige opfattelser af throbbere og egentlig tænker jeg, for mit vedkommende (ligesom for så mange andre), at throbberen bunder rinder ud i en smule frustration, og for at bruge opgavebeskrivelsens eksempel; hvis man støder på en throbber når man er i gang med at købe et eller andet online, så kan det (måske) også være en noget nervepirrende oplevelse - det synes jeg i hvertfald. Så hvordan kan man gøre denne kommunikation af hvad der sker mere klar? Kunne man måske skrive hvad computeren rent faktisk er i gang med at bearbejde? Kunne man gøre det lidt mere *soothing* og "dejligt" at vente på at computeren, eller bare browseren, er færdig med at gøre det, der nu engang skal gøres? Jeg tror at måden, at kommunikere at maskinen sover (når throbberen) kører måske kunne virke som lidt sådan et 'chill out'-øjeblik, og at der kunne være potentiale i at gå den vej - jeg synes også, i hvertfald for mit eget vedkommende, at det er meget fint at have en timer på hvor lang tid man reelt har ventet.

***

### Referenceliste

Kevin Workman. (2023, January 2). Earth Moon Emoji Orbit Genuary p5.js Let’s Code [Video]. YouTube. https://www.youtube.com/watch?v=ZfFtXzzTwP8.

Soon, W., & Cox, G. (2020). Aesthetic Programming: A Handbook of Software Studies.

