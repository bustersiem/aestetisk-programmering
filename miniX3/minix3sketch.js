let angle = 0;
let moonSize = 60;
let orbitDis = 200;
let moons = [
  '🌕', '🌖', '🌗', '🌘', '🌑', '🌒', '🌓', '🌔'
];
let moonI = 0;

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate (45);
  console.log(windowHeight, windowWidth);
}
  
function draw() {
  background(70);
  noStroke();
  //ellipse(width/2, height/2-50, 400)
  moonThrob();
  title();
  timeCount();
  exclamation();
  sleepyZzz();
}

function moonThrob() {
  push();
  textAlign(CENTER, CENTER);
  angle++;

  if (angle % 45 == 0) {
    moonI++;
    if (moonI >= moons.length) {
      moonI = 0;
    }
  }

  let moonX =  width/2 + cos(radians(angle)) * orbitDis;
  let moonY = height/2 + sin(radians(angle)) * orbitDis;

  textSize(moonSize);
  text(moons[moonI], moonX, moonY-20);
  pop();

  // inspiration og kode: https://www.youtube.com/watch?v=ZfFtXzzTwP8
}

function title() {
  push();
  textAlign(CENTER, CENTER);
  translate(width/2, height/2);
  fill(0);
  textSize(35);
  textFont("Mynerve");
  text("PSST! The computer is sleeping now...take a moment to relax!", 0, -350);
  pop();
}

function timeCount() {
  push();
  let currentTime = int(millis() / 1000); //viser tid som sekunder (1 millisek. divideret med 1000 = 1 sek.), int == helt tal UDEN decimaler
  
  translate(width/2, height/2);
  fill(0);
  textSize(30);
  textFont("Mynerve");
  text("(Seconds wasted waiting behind these pixels: " + currentTime + ")", -285, 315);
  pop();
}

function exclamation() {
  push();
  textAlign(CENTER, CENTER);
  translate(width/2, height/2);
  fill(0);
  textSize(25);
  textStyle(ITALIC);
  textFont("Mynerve");
  text("..goddamn", 195, 345);
  pop();
}

function sleepyZzz() {
  push();
  translate(width/2, height/2-40);
  fill(0);
  textSize(80);
  textFont("Mynerve");
  text("Zzz", -75, 40);
  pop();
}
  
