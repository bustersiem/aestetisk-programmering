class HappySmiley {
  constructor() {
    this.posX = windowWidth / 2;
    this.posY = windowHeight / 2;
    this.size = 370;
    this.faceCol = ("#FFDBAC");
    this.faceBorder = ("#FFCA85");
    this.irisCol = ("#0032E6");
  }

  show() {
    strokeWeight(3);
    stroke(this.faceBorder);
    fill(this.faceCol);
    ellipse(this.posX, this.posY, this.size); // ansigtet

    strokeWeight(8);
    fill(this.faceCol);
    arc(this.posX, this.posY + 50, 190, 115, radians(15), radians(165)); // smil
    
    strokeWeight(3);
    fill(255);
    ellipse(this.posX - 80, this.posY - 50, 100, 130); // venstre øje
    ellipse(this.posX + 80, this.posY - 50, 100, 130); // højre øje
    
    // venstre iris - funktionalitet for at følge mus
    let leftMouseX = constrain(mouseX, this.posX - 95, this.posX - 65);
    let leftMouseY = constrain(mouseY, this.posY - 80, this.posY - 20);
    strokeWeight(8);
    stroke(this.irisCol);  // blå øjne
    fill(0);
    ellipse(leftMouseX, leftMouseY, 35); //venstre øje følger mus + størrelse på pupillen
    
    // højre iris - funktionalitet for at følge mus
    let rightMouseX = constrain(mouseX, this.posX + 65, this.posX + 95);
    let rightMouseY = constrain(mouseY, this.posY - 80, this.posY - 20);
    strokeWeight(8);
    stroke(this.irisCol);
    fill(0);
    ellipse(rightMouseX, rightMouseY, 35); //højre øje følger mus + størrelse på pupillen
  }
}

class CryingSmiley {
  constructor(tempX, tempY) {
    this.posX = tempX;
    this.posY = tempY;
    this.size = 200;
    this.faceCol = ("#FFDBAC");
    this.faceBorder = ("#FFCA85");
    this.irisCol = ("#0032E6");
  }

  show() {
    strokeWeight(3);
    stroke(this.faceBorder);
    fill(this.faceCol);
    ellipse(this.posX, this.posY, this.size); // ansigtet

    strokeWeight(4);
    fill(this.faceCol);
    arc(this.posX, this.posY + 95, 140, 155, radians(215), radians(-35)); // ked af det mund
    
    strokeWeight(3);
    fill(this.faceCol);
    ellipse(this.posX - 40, this.posY - 30, 60, 75); // venstre øje
    arc(this.posX - 40, this.posY - 30, 95, 40, radians(35), radians(145)); // venstre øjenlåg
    ellipse(this.posX + 40, this.posY - 30, 60, 75); // højre øje
    arc(this.posX + 40, this.posY - 30, 95, 40, radians(35), radians(145)); // højre øjenlåg
  }
}