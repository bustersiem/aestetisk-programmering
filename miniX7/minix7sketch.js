let happySmiley;
let smileys = [];

function setup() {
  createCanvas(windowWidth, windowHeight - 10);
  frameRate(12)

  happySmiley = new HappySmiley();
}

function draw() {
  background(0, 200, 255);
  title();

  happySmiley.show();

  // checker om musen er i smileyen
  let distance = dist(
    mouseX, mouseY, 
    happySmiley.posX, happySmiley.posY
    );
  if (distance < 185) {
    swear();
  }

  for (let i = 0; i < smileys.length; i++) {
    smileys[i].show();
  }
}

function swear() {
  rectMode(CENTER);
  stroke(0);
  rect(happySmiley.posX, happySmiley.posY + 80, 205, 50);
  textAlign(CENTER);
  fill(255);
  textSize(35);
  text("!@#&?$*%€", happySmiley.posX, happySmiley.posY + 95);
}

function title() {
  push();
  textAlign(CENTER);
  fill(255);
  stroke(1);
  textFont("Caveat Brush");
  textSize(75);
  text("The Faciality Machine 2.0", windowWidth / 2, 135);  
  fill("#FFD528");
  pop();
}


// Når der trykkes skal der laves en grædende smiley på musens position
function mousePressed() {
  let mouseSmiley = new CryingSmiley(mouseX, mouseY);
  smileys.push(mouseSmiley);
}
