# MiniX7: Genbesøg ved MiniX2 (Geometric Emoji)

#### Faciality Machine 2.0

<img src="MiniX7.png" width=windowWidth>

Her er min [miniX7](https://bustersiem.gitlab.io/aestetisk-programmering/miniX7/index.html), det tilhørende link til min [source coden](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX7/minix7sketch.js) og filen til mine [smiley-classes](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX7/Smileys.js).

### ReadMe

I dette genbesøg ved min gamle miniX (som er miniX2) var min tanke, at jeg ville udfordre de udtalelser der er med i grundbogen, *Aesthestic Programming*, hvor Soon and Cox (2020) skriver om at emojis er "overcoded" og, at de i store træk er en repræsentation af at (hvide) europæer spredte det de kalder "facialization": 


> (...) the face — what they called the “facial machine” — is tied to a specific Western history of ideas (e.g. the face of Jesus Christ). This, in turn, situates the origins of the face with white ethnicity (despite Jesus’s birthplace) and what they call “facialization” (the imposition onto the subject of the face) has been spread by white Europeans, and thus provides a way to understand racial prejudice (...) (s. 67)

Idéen er, at der er en helt "generic" emoji - hvilket det ikke er, eftersom den jeg har valgt en emoji farve som er en smule generaliserende for den hvide europæer, som egentlig har et sæt skandinavisk blå øjne - der igen, også er meget generaliserende - som ikke ændrer sig. Det jeg har gjort er at jeg har lavet en funktionalitet, hvor emojien kan skifte dets glade og dejlig venlige udtryk, til det klassiske sure, bandende og dømmende udtryk som man ser i stor stil i kommentartråde og generelt på de social medier. Jeg har døbt mit program *the Faciality Machine 2.0*, da den, som sagt er en moderne version af Deleuze og Guattaris idé om dette universelle ansigt som er blevet **pålagt** især i mange vestlige kulture. Det handler derfor i stor stil om en fortolkning af den type mennesker man kan møde i den vestlige del, hvis ikke overalt i verden, der umiddelbart kan se venlige og gæstfrie ud, men med et knips med fingrene kan blive aggressive og sure; det er i sin ret både en provokation af kritikken om den generalisering der findes af emojier, men også en kritik af det moderne menneske og de såkaldte *keyboard warriors* anno 2023. Hvis man trykker et vilkårligt sted på kanvasset bliver der tegnet et nyt, grædende emoji - som ligesom skal illustrere den skade, som online-adfærd kan foråsage andre mennesker. Generelt er dette værk selvfølgelig en meget sort-hvid fortolkning af hvad der sker på online fora, Instagram, Facebook eller hvor end man lige får sit daglige 'fix' af internet-samvær, det er dog her at Deleuze og Guattaris tanker om generaliseringen af ansigter kommer til udtryk. Samtidig kan man snakke om at individualiteten og det unikke er taget helt ud af mine emojis; og de kan udtrykker også kun tre ting: "Glad" – sur, eller såret og ked af det. Samtidig kan man også snakke om selve classes; og hvordan man laver en specifik `class` til et objekt og at man kan lave et *instance* af det her objekt, kan måske også ses som lidt generaliserende. Det hænger egentlig meget godt sammen med Deleuze og Guattaris idé om the Faciality Machine som også generaliserer ansigter til en meget ekstrem grad; det samme gør classes - man laver en specifik `class` der ALTID, uanset hvad, ser ud på en måde, om det er emojis, et spiller objekt eller noget helt tredje, er classes netop et meget godt eksempel på netop at putte ting (eller personer) i en bestemt boks: Genstande eller objekter får et label på og de er nu denne, igen, specifikke ting.

### RunMe

Måden jeg har ændret og forbedret min kode på, er ved at jeg lavet de forskellige emojis til deres egne classes, det vil sige at i en seperat `.js`-fil har jeg lavet en `HappySmiley`- og `CryingSmiley`-class, der har en funktionalitet `show()` hvor alle de geometretiske elementer så som `ellipse` og `arc` står i. Denne kalder jeg således i min `sketch`-fil, efter jeg har kaldt dem som variabel i setup:
```
let happySmiley;

function setup() {
    (...)
    happySmiley = new HappySmiley();
}
```
Nu kan det nye objekt, som er et *instance* af `HappySmiley`, bruges ved at kalde `happySmiley.show()` i `draw()`-funktionen. Ellers har jeg optimeret min kode ved at lave diverse funktionaliteter om til reelle funktioner, der står *udenfor* `draw()`, så det er nemmere at skelne hvad de forskellige linjer kode gør. For eksempel gør `swear()`, at der skal vises en tekstboks med "bandeord" på `happySmiley`s `.posX` og `.posY` - hvilket står for sig selv, men bliver kaldt i et if-statement i draw hvis variable `distance` er mindre end 185 (som er `happySmiley`s radius). Hvis koden eksekverer bliver `swear()` kørt og der kommer en slags 'censur' på de grimme ord som emojien, eller keyboard-krigeren, bruger.

For at tegne de grædende emojis har jeg lavet en funktion med `mousePressed`, så hver gang der klikkes med musen på kanvasset bliver der tegnet et nyt, knap så glad, emoji. For at kunne tegne de nye emojis har jeg lavet en variabel `mouseSmiley`, der laver de nye instances af min class, `CryingSmiley` ved musens x- og y-positioner:

```
function mousePressed() {
  let mouseSmiley = new CryingSmiley(mouseX, mouseY);
  smileys.push(mouseSmiley);
}
```
Jeg har i det globale *scope* lavet en tom array `smileys` som skal indeholde de nye emojis, der bliver tilføjet ved hjælp af `.push` i funktionen; objektet der bliver tilføjet til arrayen, `mouseSmiley`, er egentlig bare varaiblen der, som sagt, laver et nyt instance af `CryingSmliey`-classen. For at `smileys`-arrayen overhovedet bliver vist på kanvasset har jeg i `draw()` indsat en for-loop, der looper i gennem `smileys`-arrayen:

```
for (let i = 0; i < smileys.length; i++) {
  smileys[i].show();
}
```

***

#### Referenceliste

Soon, W., & Cox, G. (2020). Aesthetic Programming: A Handbook of Software Studies.
