# MiniX8: Flowchart

### Individuelt flowchart

![]() <img src="MiniX8.png" width=800>

Jeg har valgt at lave et flowchart af min miniX6, som var et spil; det virkede naturligt at vælge denne miniX både grundet kodens kompleksitet, men også fordi det virkede som det bedste valg hvis man skulle prøve at visualiserer de forskellige *conditions* og what-not, som kan blive "triggeret" i spillet. Hvis man også skulle se på det i et større perspektiv (hvis man tænker ud over lektier og opgaver), har jeg planer om at tilføje flere "features" og *Quality of Life*-ændringer, hvis man da kan kalde dem det, til spillet i min fritid; jeg mener, at et flowchart ville hjælpe med at øge min egen forståelse for hvor de her ændringer eventuelt skulle laves! <br>
I min miniX - eller mit flowchart - har jeg valgt at inkludere de mest centrale funktioner i mit spil; det vil sige det spillet går ud på i form af at man skal undvige de har "skraldeøer", som jeg har kaldt modstander-objekter i mit flowchart, og ekstra liv-objekter, da de også spiller en stor rolle i spillet og var en stor del af tankerne jeg gjorde mig da jeg lavede spillet!

### ReadMe

Når man snakker om algoritmer, som for eksempel Facebooks EdgeRank eller Googles navnebror PageRank, som Cox og Soon (2020) bruger som eksempler, bliver der snakket om algoritmer som et "objekt" eller samlinger (se: *assemblages*), som de kalder dem, er det flydende og kontekstuelle og modeller. De skriver som følgende: 

> These operations are not fixed, but are contextual and fluid, part of larger, socio-technical assemblages, and infrastructures that are also constantly evolving and subject to variable conditions. (s. 220)

Derfor kan må man påstå, at det gør det svært at beskrive og visualisere flowcharts på en simple måde ud fra et kommunikations perspektiv - i hvertfald hvis vi holder os til tanken om at opretholde den her kompleksitet som man finder ved *the algorithmic procedural level* i algoritmer som Googles og Facebooks. Hvis man ser nærmere på hvordan man skulle lave flowcharts om de her komplekse, evigt skiftende, milliard-algoritmer skriver Cox og Soon:

> What we end up with are speculative geometries, self-organizing forms, and diagrammatic processes that reflect dynamic forces. The diagram is an “image of thought,” in which thinking does not consist of problem-solving but — on the contrary — problem-posing. (s. 221)

Altså, vi kan ikke, uden at det bliver gætværk og spekulation, håbe på at skabe flowcharts (eller "diagrammer") der reflekterer og afspejler de her "*dynamic forces*" som algoritmerne i sin essens er; ***men*** det er selvfølgelig en noget anden, hvis ikke en helt modsat, snak når det kommer sig til spil. Det er et fastsat sæt regler i spil, der udgør essensen af et spil, som under alle omstændigheder skal overholdes, og de her regler er oplagt at visualisere i gennem et flowchart. Som jeg skrev om tidligere, gør et flowchart det nemmere for andre at forstå de forskellige *condtions* (det vil sige reglerne) i et givent stykke kode eller, mere præcist, i et spil.

Som sagt skaber flowcharts efter min mening et virkelig godt overblik over *flowet* i et stykke kode, som er super brugbart når det kommer til at lave ændringer i et program eller implementere nye "features" eller funktioner. Uanset om de nye tilføjelser har indflydelse på de oprindelige funktionaliteter eller om de kan eksekvere uafhængigt af andre linjer i koden, kan det stadig hjælpe en til at forstå hvad der skal med af conditions (altså regler), og hvis nu funktionen desværre ikke virker, kan et flowchart muligvis også hjælpe med at skære de forskellige dele ud i pap og danne en idé om hvor det kan være gået galt! I min miniX6 brugte jeg selv et mindmap til at skrive alle mine idéer ned: Hvad skulle spillet handle om, hvad var målet, og til dels, budskabet med spillet, hvilke "fjender" kunne gøre spillet spændende, blandt mange andre ting, og hvis jeg havde tænkt på at lave flowchart efterfølgende så tror jeg at processen om at få skrevet alt koden til mit spil ville have været noget "nemmere" og mere flydende!

### Gruppe flowchart

Her er linket til gruppedelen af [miniX8](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/tree/main/MiniX_8).

***

#### Referenceliste

Soon, W., & Cox, G. (2020). Aesthetic Programming: A Handbook of Software Studies  (pp. 211-226). https://aesthetic-programming.net.


