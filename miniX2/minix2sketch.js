let checkbox;

function setup() {
	createCanvas(1000,750);
  background(0,200,255);
}

function draw() { // Happy smiley

  fill(0);
  stroke(3);
  textFont('Brush Script MT');
  textSize(75);
  text('The Smiley Face', width/2-212.5, 195);  
  fill('#FFD528');
  stroke(3);
  textFont('Brush Script MT');
  textSize(75);
  text('The Smiley Face', width/2-212.5, 193);  

  strokeWeight(1.5);
  fill('#FFD528'); // Smiley-yellow color fill
  ellipse(width/2,height/2,235); // Inner smiley-yellow ellipse, måske tilføj 3D vha. WEBGL
  arc(width/2,height/2+10,130,95,radians(5),radians(175)); // Smile
  fill(255,255,255); // White color fill
  ellipse(width/2-50,height/2-35,35,45); // Left eye ellipse
  ellipse(width/2+50,height/2-35,35,45); // Right eye ellipse
  fill(0,50,230); // Blue color fill (iris)
  ellipse(width/2-48.5,height/2-32.5,20,25); // Left iris ellipse
  ellipse(width/2+48.5,height/2-32.5,20,25); // Right iris ellipse
  fill(0);
  ellipse(width/2-48,height/2-32,5);
  ellipse(width/2+48,height/2-32,5);

  if (mouseIsPressed === true) { // Sad smiley
    strokeWeight(2);
    fill('#FFD528'); // Smiley-yellow color fill
    ellipse(width/2,height/2,235); // Inner smiley-yellow ellipse, måske tilføj 3D vha. WEBGL
    arc(width/2,height/2+85,130,75,radians(205),radians(-25)); // Smile
    fill(width/2,255,255); // White color fill
    ellipse(width/2-50,height/2-35,35,45); // Left eye ellipse
    ellipse(width/2+50,height/2-35,35,45); // Right eye ellipse
    fill(0,50,230); // Blue color fill (iris)
    ellipse(width/2-48,height/2-25.5,20,25); // Left iris ellipse
    ellipse(width/2+48,height/2-25.5,20,25); // Right iris ellipse
    fill(0);
    ellipse(width/2-45,height/2-22,5);
    ellipse(width/2+45,height/2-22,5);

    fill(0);
    stroke(3);
    textFont('Brush Script MT');
    textSize(75);
    text('You poked the...', width/2-185, 117);  
    fill('#FFD528');
    stroke(3);
    textFont('Brush Script MT');
    textSize(75);
    text('You poked the...', width/2-185, 115); 

    console.log("Av! Du prikkede mig...");
    
  }
}
