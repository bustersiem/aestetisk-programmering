## MiniX2: Geometric Emoji

<img src="MiniX2.png" width="500" height="500">


### RunMe

I denne miniX var opgaven at vi skulle lede efter shapes, geometri og andre syntakser fra p5.js referencelisten, og designe to tilhørende smileyer. 

Jeg tænkte med det samme at jeg ville *prøve* at benytte med af `mouseIsPressed`-variablen, så der egentlig kun skulle vises én smiley i miniX'en, men når der så trykkes på touchpaden eller musen skulle smileyen skifte udtryk. Derudover ville jeg også give mig selv en ekstra udfordring til denne miniX (efter at jeg havde sat målet lidt lavt i miniX1): Den interaktive smiley skulle være 3D - det kan man gøre ved hjælp af `WEBGL mode`, som er et parameter man kan bruge i `ellipse()`.

Her er det endelige resultat af min [miniX2](https://bustersiem.gitlab.io/aestetisk-programmering/miniX2/index.html) (og linket til min [source code](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX2/minix2sketch.js)).

### ReadMe

Som jeg skrev tidligere, ville jeg gå efter at lave en miniX der skulle være en **lille** smule interaktiv, og det lykkedes! Som jeg lidt havde forventet kunne jeg bruge `mouseIsPressed` til at vise få min smiley til at ændre udtryk - jeg var bekendt med variablen før (fra tidligere kodnings-erfaring), og det hjalp også at vi kort fik den introduceret til mandagens forelæsning. Uden at gå alt for meget i dybten fungerer variablen således, at den kode der er skrevet i `draw()`-funktion kører indtil musen bliver trykket, hvorefter koden der er i et `if ()`-statementet (som har `mouseIsPressed` som condition) bliver vist for hver frame hvor musen er trykket.

Herunder kan `if()`-statementet fra min kode ses:

```
function draw() {

    strokeWeight(2.5);
    fill('#FFD528');
    ellipse(width/2,height/2,235);
    arc(width/2,height/2+10,130,95,radians(5),radians(175));
    fill(255,255,255);
    ellipse(width/2-50,height/2-35,35,45);
    ellipse(width/2+50,height/2-35,35,45);
    fill(0,50,230);
    ellipse(width/2-48.5,height/2-32.5,20,25);
    ellipse(width/2+48.5,height/2-32.5,20,25);

    if (mouseIsPressed === true) {
        strokeWeight(2.5);
        fill('#FFD528'); 
        ellipse(width/2,height/2,235);
        arc(width/2,height/2+85,130,75,radians(195),radians(-15));
        fill(width/2,255,255);
        ellipse(width/2-50,height/2-35,35,45);
        [...]
    }
}
```

Koden inde i selve `if()`-statementet er næsten den samme kode som i `draw()` - det er den triste smiley som er den ene af de to smileyer som opgaven lød på at vi skulle kode; den første del er den anden.

Ellers har jeg brugt relativt simple former og geometrier i opgaven, f.eks. har jeg brugt `ellipse()` til både hovedet, øjnene og iriserne på smileyerne, derudover har jeg brugt `arc()` til at lave mundene på smileyerne. `arc()` tager forresten to tal, der definerer vinklen på linjen, såvel som start og stop, som femte og sjette parameter; disse tal speciferes i `radians()`. Jeg må med hånden på hjertet sige, at jeg havde lidt svært ved at forstå variablen, men efter lidt frem og tilbage (og nok ved hjælp af lidt held) endte med lykkes. For at illustrere de parametre som referencelisten siger at variablen kan tage (hvor jeg kun brugte seks ud af de otte mulige), for at blive defineret, har jeg sat syntaksen ind herunder:

```
arc(x, y, w, h, start, stop, [mode], [detail])
```

Jeg håber at det gav et lille indblik i tankerne omkring min kode, og at det måske kan hjælpe til at inspirere andre i deres kreation af smiley-faces!

---

Nu vil jeg gerne skrive lidt om mine emojis i en bredere, mere social og kulturel, kontekst; og jeg må ærlig talt indrømme, at det ikke var de største tanker jeg gjorde mig, da jeg sad og skrev koden eller for den sags skyld da jeg sad med tankerne om hvordan mit produkt skulle se ud.

Nu hvor jeg tænker over det, tror jeg egentlig at min miniX ville kunne repræsentere dét mange mennesker i samfundet forventer af andre - eller i hvertfald det der som samfund er normerne. Det jeg tænker på er den her meget sort-hvide tankegang til hvordan mennesker kan opfører sig og udstråle. Man kan opfatte andre menneskers humør på mange forskellige måder, og det jeg mener med det jeg skriver er, at når man opfatter andre som lidt 'grumpy' eller 'over-the-top' glade - måske ligefrem lalleglade - kan der ligge mange andre lag bag, og det er nok i realiteten ikke så this-or-that, men en mellem ting. Igen vil jeg pege på, at jeg originalt ikke havde lagt nogle tanker bag det her, og det er nok lidt af et såkaldt *reach*, men hvis jeg skulle sætte lidt filosoferende ord på det, skulle det nok være i den her stil! Håber ikke at det lyder for fjollet, det var bare et lille forsøg på at løse opgaven...
