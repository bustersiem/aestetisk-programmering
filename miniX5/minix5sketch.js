let phonicVowel = [
  "AL-FAH", "ECK-OH", "OSS-CAH", "ECKS-RAY", "IN-DEE-AH", "YOU-NEE-FORM", "YANG-KEY"
];

let phonicConsonant = [
  "BRAH-VOH", "CHAR-LEE", "DELL-TAH", "GOLF", "MIKE", "HOH-TEL", "PAH-PAH", "KEY-LOH", "LEE-MAH", "FOKS-TROT",
  "JUH-LEE-ET", "NO-VEM-BER", "ROW-ME-OH", "SEE-AIR-RAH", "KEH-BECK", "TANG-GO", "VIK-TAH", "WISS-KEY", "ZOO-LOO"
];

let x = -20;
let y = -5;
let rndInt = randomIntFromInterval(1, 104);
let spacingY = 25;
let spacingX = 25;

function setup() {
  frameRate(10);
  background(0);
  console.log(width, height);
  createCanvas(windowWidth, windowHeight);
} 

function draw() {
  textGenerator();
}

function textGenerator() {
  rndInt = randomIntFromInterval(1, 104);
  //console.log(rndInt); // hvilket tal blev valgt i tal-"generatoren"
  textSize(30);
  textFont("Allerta Stencil");

  if (rndInt <= 28) {
    fill(random(50, 155));
    let phonicWord = random(phonicVowel)
    text(phonicWord, x, y);
    let phonicWidth = textWidth(phonicWord);
    x += phonicWidth + spacingX;
  } else {
    fill(0, random(50,135), 0);
    let phonicWord = random(phonicConsonant)
    text(phonicWord, x, y);
    let phonicWidth = textWidth(phonicWord);
    x += phonicWidth + spacingX;
  } 

  if (x > width) {
    x = -20;
    y += spacingY;
  }
}

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
