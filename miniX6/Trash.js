class Trash {
  constructor() {
    this.speed = floor(random(3, 6));  // hastigheden på skraldet (floor: tallet kommer som en integer, dvs. et afrundet til)
    this.posX = width + 60;
    this.posY = random(0, height);
    this.size = {w: 99, h: 102}; // original pixel størrelse: 852 × 886
  }

  show() {
    image(trashImg,
      this.posX,
      this.posY,
      this.size.w,
      this.size.h
      );
  }

  move() {
    this.posX -= this.speed;  // skraldets bevægelse mod 0 på y-aksen
  }
}