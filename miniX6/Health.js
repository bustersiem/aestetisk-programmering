class Health {
  constructor() {
    this.speed = floor(random(2, 4));  // hastigheden på skraldet (floor: tallet kommer som en integer, dvs. et afrundet til)
    this.posX = width + 60;
    this.posY = random(0, height);
    this.size = {w: 85, h: 77}; // original pixel størrelse: 684 × 628
  } 

  show() {
    image(livesImg,
      this.posX,
      this.posY,
      this.size.w,
      this.size.h
      );
  }

  move() {  
    this.posX -= this.speed;  // de ekstra livs bevægelse mod 0 på y-aksen
  }
}