let trashImg, playerImg, livesImg;

let heartColor1;
let heartColor2;
let heartColor3;

let player;

let health = [];
let min_health = 1;
let lives = 3; // bestemmer hvor mange liv spilleren starter med

let trash = [];
let min_trash = 4; // hvor mange skraldeøer der er i spillet

let dodged = 0; // antallet af undviget skralde øer
let level = 1

function preload() { 
    playerImg = loadImage("boat.png");
    trashImg = loadImage("trash.png");
    livesImg = loadImage("lives.png");
}

function setup(){
    createCanvas(windowWidth, windowHeight - 5); // når man bevæger sig ned af y-asken flytter skærmen sig (windowHeight - 100?)
    background("#2B65EC"); // havblå-baggrund
    heartColor1 = color("#C51104"); // styrer farverne for de tre hjerter (skiftevis rød--grå)
    heartColor2 = color("#C51104"); // styrer farverne for de tre hjerter (skiftevis rød--grå)
    heartColor3 = color("#C51104"); // styrer farverne for de tre hjerter (skiftevis rød--grå)
    player = new Player();

    for (let i = 0; i < min_trash; i++) {
        trash.push(new Trash());
    }
}

function draw() {
    background("#2B65EC");
    player.show();
    checkHit();
    showTrash();
    checkTrashNum();
    showHealth();
    checkHealthNum();
    checkLivesLeft();
    checkLostHealth();
    checkDodged();
    levelUp();
    textAlign(CENTER);
    fill(0);
    textSize(40);
    text("LEVEL " + level, width / 2, 50);
    heart(windowWidth / 2 - 40, 75, 30, heartColor1);
    heart(windowWidth / 2, 75, 30, heartColor2);
    heart(windowWidth / 2 + 40, 75, 30, heartColor3);

    if (keyIsDown(UP_ARROW)) {
        player.moveUp();
    } else if (keyIsDown(DOWN_ARROW)) {
        player.moveDown();
    } // frem- og tilbage-funktionalitet
    /* else if  (keyIsDown(LEFT_ARROW)) {
        player.moveLeft();
    } else if (keyIsDown(RIGHT_ARROW)) {
        player.moveRight();
      
    } */
}

function heart(x, y, size, col) {
    beginShape();
    fill(col)
    strokeWeight(1)
    vertex(x, y);
    bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
    bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
    endShape(CLOSE);
  }
  

function checkTrashNum() {
    if (trash.length < min_trash) { // hvis der er mindre end det mindste antal af biler tilføjes der flere
        trash.push(new Trash()); //skaber et nyt trash-instance (trash.js)
    }
}

function showTrash() {
    // viser skralde øer og flytter skraldet på tværs af skærmen (y -= ) - se move() i trash.js 
    for (let i = 0; i < trash.length; i++) { // trash.length definerer antallet af skralde øer
        trash[i].show();
        trash[i].move();
    }
}

function checkHit() { //tjekker om spilleren bliver ramt af et skralde-objekt
    let distance;
     
    for (let i = 0; i < trash.length; i++) {
        distance = dist( // udregner afstanden mellem skibet og skraldet
            player.posX,
            player.posY,
            trash[i].posX,
            trash[i].posY
            );
  
        if (distance < player.size.w / 2) { // prøv med - 15 pixels måske??
            trash.splice(i, 1);
            lives--;
            trash.push(new Trash());
            console.log("You got hit! You have " + lives + " lives left.");
        } 
    }

    for (let i = 0; i < health.length; i++) {
        distance = dist( // udregner afstanden mellem skibet og skraldet
            player.posX,
            player.posY,
            health[i].posX,
            health[i].posY
            );
  
        if (distance < player.size.w / 2 && lives < 3) { // prøv med - 15 pixels måske??
            health.splice(i, 1);
            lives++;
            health.push(new Health());
            console.log("You gained a life! You now have " + lives + " lives!");
        } else if (distance < player.size.w / 2) {
            health.splice(i, 1);
            health.push(new Health());
            console.log("Thanks for picking me up!");
        }
    }
}

function checkHealthNum() {
    if (health.length < min_health) { // hvis der er mindre end det mindste antal af ekstra liv tilføjes der flere
        health.push(new Health()); //skaber et nyt health-instance (health.js)
    }
}

function showHealth() {
    // viser drunkende folk og flytter dem på tværs af skærmen (y -= ) - se move() i health.js 
    for (let i = 0; i < health.length; i++) { // health.length definerer antallet af ekstra liv i spillet
        health[i].show();
        health[i].move();
    }
}

function checkLivesLeft() {
    if (lives === 3) {
        heartColor1 = color("#C51104");
        heartColor2 = color("#C51104");
        heartColor3 = color("#C51104");
    }

    if (lives === 2) {
        heartColor1 = color("#6f6f6f");
        heartColor2 = color("#C51104");
        heartColor3 = color("#C51104");
    } 
    
    if (lives === 1) {
        heartColor1 = color("#6f6f6f");
        heartColor2 = color("#6f6f6f");
        heartColor3 = color("#C51104");
    } 
    
    if (lives === 0) { // når spilleren har mistet sit sidste liv --> game over!
        heartColor1 = color("#6f6f6f");
        heartColor2 = color("#6f6f6f");
        heartColor3 = color("#6f6f6f");
        noLoop();
        console.log("That was your last life. Game over...")
        textAlign(CENTER);
        fill(0);
        textSize(35);
        text("There's too much trash in the ocean...", width / 2, height / 2 + 17.5);
        text("Better luck on the next planet!", width / 2, height / 2 - 17.5);
    }
}

function checkLostHealth() {
    for (let i = 0; i < health.length; i++) {
        if (health[i].posX < -100) {
        health.splice(i,1);
        console.log("I'M GOING TO DROWN!");
        // hvis spilleren misser et ekstra liv fjernes dét objekt fra arrayen (og skal tilføjes til scoren, som er dodged)
        }
    }
}

function checkDodged() {
    for (let i = 0; i < trash.length; i++) {
        if (trash[i].posX < -100) {
        dodged++;
        trash.splice(i,1);
        console.log("Dodged trash-islands: " + dodged);
        // hvis spilleren undviger skraldet fjernes dét objekt fra arrayen (og skal tilføjes til scoren, som er dodged)
        }
    }
}


function levelUp() { // når skralde øerne bliver undveget --> niveauet stiger (og der spawner flere ekstra liv)...
    if (dodged === 4 ) {
        min_trash = 6;
        level = 2;
    } else if (dodged === 10) {
        min_trash = 7;
        level = 3;
    } else if (dodged === 17) {
        min_trash = 8;
        level = 4;
    } else if (dodged === 25) {
        min_trash = 9;
        level = 5;
    } else if (dodged === 34) {
        min_trash = 10;
        level = 6;
    } else if (dodged === 44) {
        min_trash = 11;
        level = 7;
    } else if (dodged === 55) {
        min_trash = 12;
        level = 8;
    } else if (dodged === 67) {
        min_trash = 13;
        level = 9;
    } else if (dodged === 80) {
        min_trash = 14;
        level = 10;
    } else if (dodged === 94) {
        min_trash = 15;
        level = 11;
    } else if (dodged === 109) {
        min_trash = 16;
        level = 12;
    } else if (dodged === 125) {
        min_trash = 17;
        level = 13;
    } else if (dodged === 142) {
        min_trash = 18;
        level = 15;
    } else if (dodged === 160) {
        min_trash = 19;
        level = 16;
    } else if (dodged === 179) {
        min_trash = 20;
        level = 17;
    } else if (dodged === 199) {
        min_trash = 21;
        level = 18;
    } else if (dodged === 220) {
        min_trash = 22;
        level = 19;
    } else if (dodged === 245) {
        min_trash = 30;
        level = 20;
    } else if (dodged === 290) {
        min_trash = 35;
        level = 21;
    } else if (dodged === 340) {
        min_trash = 45;
        level = 22;
    } else if (dodged === 400) {
        min_trash = 60;
        level = "EARTH SAVER!!";
    }
}
 
 
