# MiniX9: Vocable Code

### Lineage of Thrones

Her er linket til vores gruppes [miniX9](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/tree/main/MiniX_9) på Kevins GitLab.

![]() <img src="miniX9.gif" width=800>



```
show() {
    textSize(this.size);
    gradient -= 2;
    fill(data.houses[housePicker].colR,data.houses[housePicker].colG,data.houses[housePicker].colB,gradient)
    text(data.houses[housePicker].members[memberCounter], this.x, this.y);
}

```

```
{
    "description": "Decendants of the main Houses of the Patriarchal line in Game of Thrones",
    "source": "https://awoiaf.westeros.org/index.php/Houses_of_Westeros",
    "houses": [
        {
            "family": "Baratheon",
            "colR": 246,
            "colG": 239,
            "colB": 76,
            "members": [
                "Orys One-Hand",
                "Axel",
                "Reginald",
                "Boremund",
                "Davos"
            ]
        },
        {
            "family": "Stark",
            "colR": 210,
            "colG": 198,
            "colB": 182,
            "members": [
                "Torrhen the King who Knelt",
                "Rickon",
                "Cregan the Wolf of the North",
                "Donnel",
                "Donner"
            ] 
        }
    ]
}
```
