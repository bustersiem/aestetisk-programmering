let ellipseX = 0;
let accelarationX = 5; // Accelaration (lower === faster)
let fr = 60; // Starting FPS
let clr;

function setup() {
  createCanvas(500,500);
  background(200);
  frameRate(fr); // Attempt to refresh at starting FPS
  clr = color(13,17,122);
  console.log("hello world");
}

function draw() {
  background(200);
  ellipseX = ellipseX + 1 * (deltaTime / accelarationX); // Move ellipse in relation to deltaTime

  if (ellipseX >= width) {
    // If you go off screen.
    if (fr === 60) {
      clr = color(255,128,0);
      fr = 50;
      frameRate(fr); // Make frameRate 50 FPS
    } else {
      clr = color(255,51,255);
      fr = 60;
      frameRate(fr); // Make frameRate 60 FPS
    }
    ellipseX = 0;
  }
  fill(clr);
  ellipse(ellipseX,250,75);
}
