# MiniX4: Data Capture

<img src="miniX4.png" width=windowWidth height=windowHeight>

Her er min [miniX4](https://bustersiem.gitlab.io/aestetisk-programmering/miniX4/index.html) og det tilhørende link til min [source code](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX4/minix4sketch.js).

### RunMe

I min kode har jeg leget lidt med webcamet. Det har jeg gjort ved at kameraet tager billeder af personen, der kan ses i billedet indenfor et givent interval - som jeg i det her tilfælde har sat til to sekunder (men mere om det senere). 

Koden ser således ud, at jeg har startet med deklarere tre variabler: `capture`, `readyCheck` (som jeg har givet en startværdi af `false`) og `billeder`, der er en array. For at jeg overhovedet kunne tilgå webcamet i computeren brugte jeg i `setup()`-funktionen `createCapture(VIDEO)` som jeg lavede til en til en *callback-funktion* kaldet `capture` (det kan godt være, at jeg har misforstået udtrykket, men det er en mindre sag). Derefter definerede jeg størrelsen på billede-området som jeg kaldte med `.size()`. Til sidst ville jeg skulle kammeraet for brugeren, hvilket jeg gjorde ved hjælp af `.hide()` - og det var her at funktionen var "handy", da jeg bare kunne skrive navnet på den, altså `capture`, efterfulgt af `.` og dernæst det jeg ville justere.

I `draw()` skulle jeg nu have de billeder som `capture`-funktionen tager, frem på skærmen, og med lidt bakseri, fik jeg lavet de billeder capture tager til et billede på p5.js canvas - med `image(billeder[i]...);` bliver billederne i min `billeder`-array "trykt" på canvaset. Herefter gennegår `for`-loopen så min array med billeder; godt nok sker dette ovenpå hinanden. Dette er dog et bevidst valg, da det var tiltænkt at brugeren skulle kunne se, at der bliver taget billeder af dem og at de bliver "overvåget"; så deres ansigtsudtryk, øjne, osv. bliver optaget og registreret. Dette er også noget af hovedparten af det jeg prøver at kommunikere: Det skal foreslå den her data capturing der sker, i form af at brugeren bliver fotografet. Til sidst kan jeg fortælle, at måden et nyt billede så bliver tilføjet til arrayen sker et par linjer over i `if`-statementet ved hjælp af `.push(capture.get())`, som tilføjer en "del" (eller en unit) til `billeder` med `.push` - og det den tilføjer er et element fra callback-funktionen `capture`, som vi lavede tidligere. For at man overhovedet kan se at der bliver "taget billeder" af en, blev jeg nødt til at sætte denne funktion i et `if`-statement, hvor betingelsen var `frameCount` (der altid er 60 medmindre, at vi selv justerer på det) divideret eller modular (som er `%`) med 120, hvilket giver to - og det er derfor kun hvert andet sekund, at koden i dette `if`-statement eksekverer, og der bliver derfor kun taget et nyt billede hvert andet sekund.

En lille sidenote: Hvis jeg ikke have lavet dette `if`-statement, ville der være blevet taget et nyt billede konsekvent hvert sekund, og det ville derfor have lignet at det var en slags video-feed i stedet for det her "data capture" i et givent interval.

### ReadMe

#### Big Brother

Jeg har valgt mit "værk" for *Big Brother*, da jeg blev meget inspireret af både George Orwells 1984, hvor de bruger udtrykket "Big Brother is watching you" (som jeg også har taget med i min skitse), men også af Apples 1984 Macintosh-reklame, der spiller på det her totalitærer og dytopiske. I deres '*call for works*' skriver transmediale i 2015:

> "(...) the quantification of everything means that we are all contributing to a state of permanent capture of life into data. As citizens, workers and players of the networks we (often involuntarily) double as sensors for bodies of global data collection, working for the potential extraction of value everywhere and increasing the productivity of everyday life."

Og det er specielt dette med "a state of permanent capture of life into data", som jeg hang mig meget i - og jeg harr så prøvet at fortolke det vede at tage billeder af folk, når de bruger computeren; det er måske en lidt ringere form for eye-tracking, men jeg kunne i hvertfald blive meget skræmt ved tanken om at internetfirmaer som Facebook, Twitter, Nike eller hvem end det måtte være, fik data i form af billeder af mig, når de køber data fra browseren eller bare samler det selv. Det er et lille forsøg på et slags "opråb" om, at man skal være opmærksom på at man i 2023 kan blive overvåget hvor end man befinder - man kan blive overvåget mens man er på toiletten, hvis man for eksempel swiper på sin smartphone mens man gør det, eller man kan blive overvåget mens man scroller i gennem katte-videoer på 9GAG; men det er nok rimelig åbenlyst for de fleste. Pointen er, at *Big Brother* bringer en ny form for data, i form af live-captured billeder af folk live bag computeren, til verden, og de fleste er nok enten sådan rimelig okay med at deres data bliver minet eller sådan lidt "on-the-fence" (så at sige) omkring det, mens jeg tror at størstedelen af PC-brugere, eller hvilket device vi nu end taler om, har meget i mod at skulle "donere" data i form af live-billeder til Big Data-firmaerne.

Jeg vil sige, at jeg er blevet meget bedre til at lave (og navngive til dels) funktioner og callback-funktioner i denne miniX. Så har jeg også brugt og lært noget om de har egenskaber der er p5.js-biblioteket som for eksemepel `createCapture`, som jeg også har brugt i denne opgave. Generelt synes jeg at opgaven var **meget** abstrakt og egentlig rimelig svær at gribe an i forhold til de andre tre miniX's som vi har skulle lave indtil nu.

Som Shoshana Zuboff beskriver i sin bog om *surveillance capitalism* så tror jeg at falder ind under den katogori, som hun snakker i første del af et interview hun giver med hensyn til data capture (som jeg har linket til herunder), af folk, der egentlig ikke har noget i mod den store mængde data, der bliver opgivet til de har Big Data-firmaer, jeg har selv et Apple-watch og jeg tracker næsten alt hvad der kan trackes (lige med undtagelse af kalorieindtag). Og ja, jeg har egenltig heller ikke så meget i mod at få tilrettelagte reklamer - så jeg sidder lidt og tænker: Hvorfor er det her data overvågning egentlig en dårlig ting? Tjo, man kan selvfølgelig sige, at det med at man kan forudsige hvad jeg vil gøre ikke bare i den nære fremtid, men langt ud i fremtiden, som Zuboff kalder *behavioral surplus*, er en del skræmmende, og det spiller egentlig også meget godt ind i Orwells forudsigelse af hvordan et dystopisk samfund ville se ud 1984, hvor alt og alle er overvåget. Det der er vildt (og som sagt, lidt skræmmende) er, at vi egentlig ikke har nogle idéer om hvordan vi bliver overvåget. Det betyder selvfølgelig at de kulturelle implikationer af data capture kunne være at, algoritmer kunne forudsige alt mulig om os inden noget egentlig overhovedet er sket - måske at vi havde planer om at fri, at man lider af en psykisk sygdom uden at ane det eller det kunne være mange andre ting. Man kunne vel derfor nemt forestille sig, at en algoritme kunne sidde og bestemme og forudsige ting om forskellige befolkningsgrupper, og tage beslutninger baseret udelukkende på en udregning om forventet opførsel hvilket ikke ville være ideelt.

***

### Referenceliste

- Call for Works 2015 | transmediale. (n.d.). https://archive.transmediale.de/content/call-for-works-2015.

- vpro documentary. (2019, December 21). Shoshana Zuboff on surveillance capitalism | VPRO Documentary [Video]. YouTube. https://www.youtube.com/watch?v=hIXhnWUmMvw.


