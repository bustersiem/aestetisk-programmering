let capture;
let readyCheck = false;
let billeder = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(51);
  capture = createCapture(VIDEO, ready); // video-optager
  capture.size(640, 400);
  capture.hide();
}

function draw() {
  titleText();
  dataText();
  imageMode(CENTER);
  
  if (frameCount % 120 == 0) { // tager billede hvert ANDET sekund --> hver 120 frame (60 frames pr sek / 120 = 2 sek)
    billeder.push(capture.get()); // tilføjer det nye billede - som kommer hvert andet sekund - til "billeder"-arrayen
  }
  
  for (let i = 0; i < billeder.length; i++) {
    image(billeder[i], windowWidth/2, 400); // array ("billeder") viser billede - af bruger - på skærm
  }
}

function titleText() {
  push();
  textAlign(CENTER, CENTER);
  fill(0);
  textSize(75);
  textFont("Rubik Iso");
  text("Big Brother is Watching You.", windowWidth/2, 80);
  pop();
}

function dataText() {
  push();
  textAlign(CENTER, CENTER);
  fill(0);
  textSize(40);
  textFont("PT Mono");
  text("Don't get upset.", windowWidth/2, 690);
  text("We're just borrowing some data about your 'PC-usage'.", windowWidth/2, 740);
  pop();
}

function ready() {
  readyCheck = true;
}
